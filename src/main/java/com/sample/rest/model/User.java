package com.sample.rest.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;


@Data
@AllArgsConstructor
@Builder
public class User implements Comparable<User>{

	private int id;
	private String name;

	
	@Override
	public int compareTo(User o) {
		return this.getName().compareTo(o.getName());
	}
	
}
