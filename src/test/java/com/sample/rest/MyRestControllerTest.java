package com.sample.rest;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.sample.rest.controller.MyRestController;
import com.sample.rest.model.User;
import com.sample.rest.service.UserService;


/**
 * https://www.petrikainulainen.net/programming/spring-framework/unit-testing-of-spring-mvc-controllers-rest-api/
 * @author Sasmit
 *
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(MyRestController.class)
public class MyRestControllerTest {

	 
	
/*
	@Autowired
	WebApplicationContext context;
*/
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	UserService userService;

	/*@BeforeAll
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}*/

	
	/*@Test
	@DisplayName("Testing sayHello Method")
	public void testSayHello() {
		MyRestController controller = new MyRestController();
		assertEquals("Hello World !", controller.sayHello());
		
		MvcResult mvcResult =  mockMvc.perform(get("/")).andDo(print())
			.andExpect(status().isOk()).andExpect(jsonPath("$.message", "Hello World !"))
		      .andReturn();
	}*/
	
	
	@Test
	@DisplayName("Fetching all users")
	public void testFetchUsers() throws Exception {
	
		User user1 = User.builder().id(1).name("test1").build();
		User user2 = User.builder().id(2).name("test2").build();
		
		when(userService.getUsers()).thenReturn(Arrays.asList(user1,user2));
		
		
		MvcResult mvcResult =  mockMvc.perform(get("/api/v1.0/users").contentType(MediaType.APPLICATION_JSON))
	      	.andExpect(status().isOk())
	      	.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
	      	.andExpect(jsonPath("$[0].id", is(1)))
	      	.andReturn();
		
		System.out.println("----------------------------------------");
		
	}
}
